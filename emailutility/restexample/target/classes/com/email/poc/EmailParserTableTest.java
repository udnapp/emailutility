package com.email.poc;

import static org.junit.Assert.assertNotNull;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.swing.text.rtf.RTFEditorKit;

import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.microsoft.OfficeParser;
import org.apache.tika.sax.BodyContentHandler;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import com.email.parser.TransformationUtil;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;

public class EmailParserTableTest {

	@Test
	public void testEmailParse() throws Exception {
		// Create a SyncPersonType with WCS out-bound XML
		ClassLoader classLoader = getClass().getClassLoader();
		String resultText = null;
		String resultHtml = null;
		/*
		 * String xmlInput = TransformationUtil.loadFromFile(file
		 * .getCanonicalPath());
		 */
		Properties props = System.getProperties();
		props.put("mail.host", "smtp.dummydomain.com");
		props.put("mail.transport.protocol", "smtp");
		File file = new File(
				"C:\\Users\\Panduranga\\java-workspace-POC-1\\restexample\\emails\\2253700.eml");
		Session mailSession = Session.getDefaultInstance(props, null);
		InputStream source = new FileInputStream(file);
		MimeMessage message = new MimeMessage(mailSession, source);
		if (message instanceof MimeMessage) {
			MimeMessage m = (MimeMessage) message;
			Object contentObject = m.getContent();
			if (contentObject instanceof Multipart) {
				BodyPart clearTextPart = null;
				BodyPart htmlTextPart = null;
				Multipart content = (Multipart) contentObject;
				int count = content.getCount();
				for (int i = 0; i < count; i++) {
					BodyPart part = content.getBodyPart(i);
					if (part.isMimeType("text/plain")) {
						clearTextPart = part;
						// break;
					}
					if (part.isMimeType("text/html")) {
						htmlTextPart = part;
					}
				}

				if (clearTextPart != null) {
					resultText = (String) clearTextPart.getContent();
				}
				if (htmlTextPart != null) {
					String html = (String) htmlTextPart.getContent();
					System.out.println("html : " + html);
					Document doc = Jsoup.parse(html);
					boolean skipHeader = false;
					Elements links = doc.select("tr");
					boolean matchFound = false;
					for (Element link : links) {
						/*
						 * Elements th = link.select("th"); for ( int k= 0 ; k<
						 * th.size();k++){
						 * System.out.println("th "+th.get(k).text()); String
						 * type = th.get(k).text(); List[] typeList = new
						 * ArrayList[th.size()]; }
						 */
						boolean breakLoop= false;
						if (!skipHeader) {

							skipHeader = true;
							continue;
						}
						int counter = 0;
						Elements td = link.select("td");
						for(int k=0;k< td.size(); k++){
						Pattern p = Pattern
								.compile("Date|Flight|From|To|Depart|Arrive|Date<=b>|Arrive=/b>");
						Matcher match = p.matcher(td.get(k).text());
						// to handle <tr> data and check for the first occurence
						// of string
						counter++;
						if(match.matches()){
							break;
						}
						
						}
						boolean processTo = false;
						boolean processFrom = true;
						boolean processDeparture = true;
						boolean processArrival = true;
						for (int j = counter+1; j < td.size(); j++) {
						
							// System.out.println("<td> elements" +
							// td.get(j).text());
							if (LookupUtil.lookUpDateList(td.get(j).text())) {
								System.out.println("Date of journey is:::::::"
										+ td.get(j).text());
							}

							else	if (LookupUtil.lookUpFlightList(td.get(j).text())) {
								System.out.println("Flight Name is :::::::::"
										+ td.get(j).text());
							}

							else if ( LookupUtil.lookUpFromCityList(td.get(j).text())) {
								
								Pattern pattern = Pattern
										.compile("([01]?[0-9]|2[0-3]):[0-5][0-9]");
						Matcher matchFind   = 	pattern.matcher(td.get(j).text());
								if (processFrom) {
									System.out
											.println("Departure From :::::::::"
													+ td.get(j).text());
									if(matchFind.find()){
										System.out.println(" the departure time is " + matchFind.group(0)   );
										processDeparture = false;
									}
									processFrom = false;
									processTo= true;
								}
								else if (processTo) {
									System.out.println("Arrival To :::::::::"
											+ td.get(j).text());
									if(matchFind.find()){
										System.out.println(" the Arrival time is " + matchFind.group(0));
										processArrival = false;

									}
									processTo = false;
									processFrom  = true;

								}
							
							}
						
						else {
							
							
								Pattern patternTime = Pattern
										.compile("([01]?[0-9]|2[0-3]):[0-5][0-9]");
						Matcher matchFindTime   = 	patternTime.matcher(td.get(j).text());
						boolean departure = false;
						boolean arrival = true;
						if(matchFindTime.find()&& !departure){
							System.out.println(" the departure/arrival time is " + matchFindTime.group(0)   );
                         arrival = false;
						}
						/*if(matchFindTime.find() && !arrival){
							System.out.println(" the arrival time is " + matchFindTime.group(0)   );
						}*/
							
						}
							
							if(td.get(j).text().contains("Fare Summary")){
								breakLoop = true; 
								break;
							}
						}
					if(breakLoop){
						break;
					}
					}
					resultHtml = Jsoup.parse(html).text();
				}
			} else if (contentObject instanceof String) // a simple text message
			{
				String result = (String) contentObject;
			} else // not a mime message
			{
				System.out.println("do nothing");

			}
		}
		/*
		 * System.out.println("Subject : " + message.getSubject());
		 * System.out.println("From : " + message.getFrom()[0]);
		 * System.out.println("--------------");
		 * System.out.println("Body plain : " + resultText);
		 * System.out.println("\n"); System.out.println("Body html: " +
		 * resultHtml);
		 */

		assertNotNull(resultText);

	}

	/*
	 * @Test public void testPDFParse() throws Exception { FileWriter fw = new
	 * FileWriter("./pdftotext.txt"); // create buffered writer BufferedWriter
	 * bw = new BufferedWriter(fw);
	 * 
	 * // create pdf reader PdfReader pr = new
	 * PdfReader("./emailoutput/SOA-562956-AUGUST-2014.pdf"); // get the number
	 * of pages in the document int pNum = pr.getNumberOfPages(); // extract
	 * text from each page and write it to the output text file for (int page =
	 * 1; page <= pNum; page++) { String text =
	 * PdfTextExtractor.getTextFromPage(pr, page);
	 * System.out.println("text from pdf " + text); bw.write(text);
	 * bw.newLine(); assertNotNull(text); }
	 * 
	 * }
	 * 
	 * @Test public void testRTFParse() throws Exception {
	 * 
	 * FileInputStream stream = new FileInputStream(
	 * "C:\\Users\\Panduranga\\Downloads\\RTF-Spec-1.7.rtf"); RTFEditorKit kit =
	 * new RTFEditorKit(); javax.swing.text.Document doc =
	 * kit.createDefaultDocument(); kit.read(stream, doc, 0);
	 * 
	 * String plainText = doc.getText(0, doc.getLength());
	 * System.out.println("text" + plainText); assertNotNull(plainText);
	 * 
	 * }
	 * 
	 * 
	 * @Test public void testPDFparseTika() throws Exception { //change the name
	 * of file to .pdf, .rtf for( int i = 1 ; i <= 10 ; i++) {
	 * System.out.println("emails\\all_email_templates_output-"+i); }
	 * 
	 * File file = new File(
	 * "C:\\Users\\Panduranga\\Downloads\\01a_76901SagittaBenefitPointConversion.doc"
	 * ); //parse method parameters Parser parser = new AutoDetectParser();
	 * BodyContentHandler handler = new BodyContentHandler(); Metadata metadata
	 * = new Metadata(); FileInputStream inputstream = new
	 * FileInputStream(file); ParseContext context = new ParseContext();
	 * //parsing the file parser.parse(inputstream, handler, metadata, context);
	 * System.out.println("The extracted content is " + handler.toString());
	 * 
	 * 
	 * }
	 */
}