package com.email.poc;

import static org.junit.Assert.assertNotNull;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;

import javax.swing.text.rtf.RTFEditorKit;

import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.microsoft.OfficeParser;
import org.apache.tika.sax.BodyContentHandler;
import org.junit.Test;
import org.w3c.dom.Document;

import com.email.parser.TransformationUtil;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;

public class EmailPOCTest {

/*	@Test
	public void testEmailParse() throws Exception {
		// Create a SyncPersonType with WCS out-bound XML
		File folder = new File("./emailoutput");
		ClassLoader classLoader = getClass().getClassLoader();
		File folder1 = new File(classLoader.getResource("emailoutput")
				.getFile());
		for (File file : folder1.listFiles()) {
			String xmlInput = TransformationUtil.loadFromFile(file
					.getCanonicalPath());
			System.out.println("xmlInput " + xmlInput);
			assertNotNull(xmlInput);

		}
	}*/

	@Test
	public void testPDFParse() throws Exception {
		FileWriter fw = new FileWriter("./pdftotext.txt");
		// create buffered writer
		BufferedWriter bw = new BufferedWriter(fw);

		// create pdf reader
		PdfReader pr = new PdfReader("./emailoutput/SOA-562956-AUGUST-2014.pdf");
		// get the number of pages in the document
		int pNum = pr.getNumberOfPages();
		// extract text from each page and write it to the output text file
		for (int page = 1; page <= pNum; page++) {
			String text = PdfTextExtractor.getTextFromPage(pr, page);
			System.out.println("text from pdf " + text);
			bw.write(text);
			bw.newLine();
			assertNotNull(text);
		}

	}

	@Test
	public void testRTFParse() throws Exception {

		FileInputStream stream = new FileInputStream(
				"C:\\Users\\Panduranga\\Downloads\\RTF-Spec-1.7.rtf");
		RTFEditorKit kit = new RTFEditorKit();
		javax.swing.text.Document doc = kit.createDefaultDocument();
		kit.read(stream, doc, 0);

		String plainText = doc.getText(0, doc.getLength());
		System.out.println("text" + plainText);
		assertNotNull(plainText);

	}
	
	
	@Test
	public void testPDFparseTika() throws Exception {
		//change the name of file to  .pdf, .rtf
		for( int i = 1 ; i <= 10 ; i++) {
			System.out.println("emails\\all_email_templates_output-"+i);
		}
		
		/*File file = new File("C:\\Users\\Panduranga\\Downloads\\01a_76901SagittaBenefitPointConversion.doc"); 
		//parse method parameters 
	     Parser parser = new AutoDetectParser();
		 BodyContentHandler handler = new BodyContentHandler();
		 Metadata metadata = new Metadata(); 
		FileInputStream inputstream = new FileInputStream(file);
		 ParseContext context = new ParseContext(); 
		//parsing the file 
		 parser.parse(inputstream, handler, metadata, context);
		 System.out.println("The extracted content is " + handler.toString());
		*/
		
	}
}