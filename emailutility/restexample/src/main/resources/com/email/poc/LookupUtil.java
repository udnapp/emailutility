package com.email.poc;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class LookupUtil {
	static List  dateList= new ArrayList();
    static List  lookupCitiesList = new LinkedList();
    static List  lookupFlightList = new ArrayList();
    static List lookupCityList = new ArrayList();
	static {
		
		dateList.add("Jan");
		dateList.add("Feb");
		dateList.add("Mar");
		dateList.add("Apr");
		dateList.add("May");
		dateList.add("Jun");
		dateList.add("Jul");
		dateList.add("Aug");
		dateList.add("Sep");
		dateList.add("Oct");
		dateList.add("Nov");
		dateList.add("Dec");
		
		lookupFlightList.add("VF 201");
		lookupFlightList.add("VF 204");
		lookupFlightList.add("UA760");
		lookupFlightList.add("UA389");
		lookupFlightList.add("AC202");
		lookupFlightList.add("AC215");
		lookupFlightList.add("UA325");
		lookupFlightList.add("UA1456");
		lookupFlightList.add("UA871");
		lookupFlightList.add("FD3224");
		lookupFlightList.add("UA6508");
		lookupFlightList.add("UA837");
		lookupFlightList.add("NH949");
		lookupFlightList.add("NH950");
		lookupFlightList.add("UA852");
		lookupFlightList.add("UA798");
		lookupFlightList.add("UA3816");
		lookupFlightList.add("UA1419");
		lookupFlightList.add("UA1416");
		lookupFlightList.add("UA4131");
		lookupFlightList.add("UA870");
		lookupFlightList.add("UA1209");
		lookupFlightList.add("UA863");
		lookupFlightList.add("UA1409");
		lookupFlightList.add("UA1693");		
		lookupFlightList.add("UA3615");


		
		Scanner s = null;
		try {
			s = new Scanner (new File("E:\\cities.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while(s.hasNextLine()){
		String lineInfo = 	s.nextLine(); 
		String countries[]= lineInfo.split(",");
				
			lookupCitiesList.add(countries[3]);
			
		}
		
		lookupCityList.add("SFO");
		lookupCityList.add("JFK");
        lookupCityList.add("YVR");
        lookupCityList.add("YYC");
        lookupCityList.add("TX");
        lookupCityList.add("SAN");
        lookupCityList.add("KBV");
        lookupCityList.add("DMK");
        lookupCityList.add("SAN");
        lookupCityList.add("AUS");
        lookupCityList.add("NRT - NARITA");
        lookupCityList.add("MNL");
        lookupCityList.add("RIC");
        lookupCityList.add("EWR - LIBERTY");
        lookupCityList.add("BDA");
        lookupCityList.add("SYD");
        lookupCityList.add("LAS");
        lookupCityList.add("DEN");
        lookupCityList.add("OMA");
        lookupCityList.add("ORD - O'HARE");


	}
	
	public static boolean  lookUpDateList(String  date) {
		for(Object o: dateList){
			if (date.toLowerCase().contains((String)o.toString().toLowerCase()))
				return true;
		}
			
		return false;
	}
	
	public  static boolean lookUpFlightList(String flightName){
		for(Object o: lookupFlightList){
			if(flightName.contains((String)o)){
				
				return true;
			}
		}
		
		return false;
	}
	 
	
	public static boolean lookUpFromList(String cityName) throws FileNotFoundException{
		
		String  cityNames [] =  cityName.split(" ");
		for(int i = 0; i < cityNames.length; i++){
			if(lookupCitiesList.contains(" "+cityNames[i]+"\"")){
				return  true;
			}
		}
		
		
		return  false;
	}
	
	
public static boolean lookUpFromCityList(String cityName) throws FileNotFoundException{
		
	for(Object o : lookupCityList){
		if(cityName.toLowerCase().contains((String)o.toString().toLowerCase())){
			return true;

		}
	}
			
		
		return  false;
	}
}
