package com.email.poc;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.email.model.EmailInput;
import com.email.parser.TransformationUtil;

@Controller
@RequestMapping("/email")
public class EmailController {

	@RequestMapping(value = "/emailout/{id}", method = RequestMethod.PUT, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, consumes = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public String putForemail(@PathVariable("id") String emailId,
			@RequestBody EmailInput emailInput) throws Exception {
		
		String  name = emailInput.getName();
		try {

			ClassLoader classLoader = getClass().getClassLoader();
			File folder1 = new File(classLoader.getResource("emailoutput")
					.getFile());  
		
			for (File file : folder1.listFiles()) {
				String xmlInput = TransformationUtil.loadFromFile(file
						.getCanonicalPath());
				// send emailInput for processing
				// convertTo(emailInput);

				System.out.println("xmlInput " + xmlInput);
			}
		
		} catch (NumberFormatException e) {

		} catch (Exception e) {
			throw e;
		}
		return " email processed successfully for name " +name;
		}

	@RequestMapping(value = "/{email-id}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String getemailDetailsForemailId(
			@PathVariable("email-id") String emailId) throws Exception {
		try {
			// get the jaxb response and return
			EmailInput email = new EmailInput();

			return "Hellp";

		} catch (Exception e) {

			throw e;
		}

	}

}
