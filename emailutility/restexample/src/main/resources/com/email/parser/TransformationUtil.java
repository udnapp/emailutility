package com.email.parser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;

import org.springframework.core.io.ClassPathResource;





public class TransformationUtil {	
	
	public static String loadFromFile(String path) {
		//ClassPathResource resource = new ClassPathResource(path);
		StringBuffer sb = new StringBuffer();
		try {
			FileReader fr=  new FileReader(path);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null) {
				sb.append(line);
			}
			br.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		return sb.toString();
	}
	
	public static String loadFromFileXml(String path) {
		ClassPathResource resource = new ClassPathResource(path);
		StringBuffer sb = new StringBuffer();
		try {
			InputStream is = resource.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line;
			while((line = br.readLine()) != null) {
				sb.append(line).append(System.lineSeparator());
			}
			br.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		return sb.toString().trim();
	}
}
