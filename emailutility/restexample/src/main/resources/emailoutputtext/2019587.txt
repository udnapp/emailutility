---------- Forwarded message ----------
From: Crump, Jenelle <Jenelle.Crump@jetblue.com>
Date: Mon, Feb 17, 2014 at 4:51 PM
Subject: FW: myIDTravel Leisure Booking/Listing Rebooking
To: "mydogkenna@gmail.com" <mydogkenna@gmail.com>




Kind Regards,
_________________________________________________________
Jenelle Crump | Business Analyst,  Strategic Planning
JetBlue Airways    | Salt Lake Support Center (SSC) - Customer Support
o: 801.449.2243   | e: jenelle.crump@jetblue.com

-----Original Message-----
From: noreply@myidtravel.com [mailto:noreply@myidtravel.com]
Sent: Monday, February 17, 2014 3:51 PM
To: Crump, Jenelle
Subject: myIDTravel Leisure Booking/Listing Rebooking

Greetings,

Your leisure Travel rebooking was successful. Below you will find a new
copy of your itinerary.

Names: DABLING, KALE LINCOLN  MR


Booking Reference:   UNRKCP
Ticketing Airline:   JetBlue
Ticketnumbers:       279-2107038822

Flightno  Date         From  Dept*  To   Arrv*    Status  Class
B6066     25 Feb 2014  ABQ   23:59  JFK  05:52+1  listed  Economy
B6318     26 Feb 2014  JFK   06:35  BOS  07:44    listed  Economy

Online Check-in is available 24 hrs prior to departure at
www.jetblue.com/checkin for e-tickets. By checking in online, this will
place you on the standby list. When traveling on a connection, you should
only check-in for your first segment. The system will automatically place
you in the correct order on the priority list for your connection when you
are cleared in your origin city.

Please remember personal care items containing hazardous materials (e.g.
liquids, perfume, gels, aerosols) totaling 3.4oz/100 ml or less per bottle
may be carried on board the aircraft. The 3.4 oz/100 ml bottles must be in
1 quart-sized, clear, plastic, zip-top bag. Each customer is allowed 1
quart-sized bag wich must be placed in a screening bin. The 1 quart-sized
bag per person limits the total  liquid volume each customer can bring. The
3.4 oz/100 ml container size is a security measure.



* All times are local

Please mark the baggage you are checking in with your name and address.

Have a pleasant trip!

Your myIDTravel-Team
