

From: "reservas.barra@starwoodhoteis.net<mailto:reservas.barra@starwoodhoteis.net>" <reservas.barra@starwoodhoteis.net<mailto:reservas.barra@starwoodhoteis.net>>
Reply-To: "reservas.barra@sheraton.com<mailto:reservas.barra@sheraton.com>" <reservas.barra@sheraton.com<mailto:reservas.barra@sheraton.com>>
Date: Monday, February 17, 2014 9:13 PM
To: Cisco Employee <husantos@cisco.com<mailto:husantos@cisco.com>>
Subject: Sheraton Barra Hotel & Suites - Carta de Confirma��o

[Sheraton Hotel & Resorts]
[http://www.starwoodhoteis.net/sistema_confirmacao/global/cabecalho/reserve.jpg]<http://www.sheraton-barra.com.br>      [http://www.starwoodhoteis.net/starwoodhoteis.net/sistema_confirmacao/global/cabecalho/ofertas_especiais.jpg] <http://www.sheraton-barra.com.br>        <http://www.starwoodhotels.com/preferredguest/index.html> [http://www.starwoodhoteis.net/sistema_confirmacao/global/cabecalho/spg.jpg]


[Sheraton Barra Hotel & Suites]
Avenida L�cio Costa 3150 - Barra da Tijuca � Rio de Janeiro, Rio de Janeiro 22450-220 � Brasil � Telefone: +55 (21) 3139 8000
[http://www.starwoodhoteis.net/sistema_confirmacao/sheraton-barra/email_reserva.jpg]

Confirma��o: 899325926
Prezado(a) Sr.(a) Santos,

Temos o prazer de confirmar sua estada no Sheraton Barra Hotel & Suites e estamos ansiosos com a sua chegada.

Caso tenha algo mais que possamos fazer, por favor nao hesite em nos contactar.

Desejamos aos senhores uma excelente viagem!

Sintia Gomes
Gerente Geral

Sua Programa��o:
Data de Entrada:        18/2/2014
Data de Sa�da:  19/2/2014
Check In:       15h
Check Out:      12h

Acomoda��o:
Apartamento Classic
Apartamento com cozinha americana, sala de estar e quarto integrados. Possui uma cama King Size ou duas camas de solteiro estilo americano, �rea de trabalho e varanda com vista para o mar.
Nome do H�spede:        Hugo Santos
N�mero de Adultos:      1
N�mero de Crian�as:     0
N�mero de Quartos:      1
N�mero de H�spedes:     1

Di�ria:
Di�ria de:      18/2/2014 at� 19/2/2014
Tarifa: 238,50 D�lares por noite.
Internet Included. Breakfast is not included. Please add 5 percent as Governmental Tax and R$ 7,00 as Contribution Bureau Room City Tax per room per day.

0
Taxas:
Acrescentar 5% de ISS e taxa de turismo opcional de R$ 7,00.


Garantia
Pagamento direto pelo h�spede.
Esta reserva n�o est� garantida, ficar� dispon�vel at� as 16:00 horas da data de entrada. Para garanti-la, por favor nos enviar um documento de garantia de no show da empresa /agencia ou um n�mero e validade de cart�o de credito.



Observa��es:

E-mail: husantos@cisco.com<mailto:husantos@cisco.com>
Servimos diariamente o Buffet de caf� da manh� em nosso Restaurante Terral das 6h30 at� 10h30.
� obrigat�ria a apresenta��o de documento de identifica��o de crian�as ou adolescentes, menores de 18 anos, os quais devem estar acompanhados dos pais (pai e m�e) ou de respons�vel legal, ou portar termo de autoriza��o do Juizado de Menores, no caso de viagem com terceiros. (Artigo 82 - Estatuto da Crian�a e do Adolescente Lei N� 8069/90).


Como Chegar:
Do Aeroporto Internacional Tom Jobim:

  *   Pegue a Linha Vermelha para o T�nel Rebou�as;
  *   Seguir pela Lagoa e G�vea;
  *   Pegar � auto estrada Lagoa Barra;
  *   Seguir pela Avenida L�cio Costa.

Do Centro da Cidade:

  *   Prosseguir passando pelas praias do Flamengo, Botafogo, Copacabana, Leblon e G�vea;
  *   Seguir pela Lagoa e G�vea;
  *   Pegar � auto estrada Lago Barra;
  *   Seguir pela Avenida L�cio Costa

[http://www.starwoodhoteis.net/sistema_confirmacao/sheraton-barra/google_maps.jpg]<http://www.google.com.br/maps?f=q&source=s_q&hl=pt-BR&geocode=&q=Sheraton+Barra&sll=-22.894051,-43.344029&sspn=0.011781,0.022724&ie=UTF8&hq=Sheraton+Barra&hnear=&ll=-23.002012,-43.317375&spn=0.047088,0.090895&z=14>
Sua Privacidade:

Aten��o: Por medidas de seguran�a, solicitaremos um documento de identifica��o com foto no check in.

Em caso de reservas com crian�as entre 0 e 18 anos incompletos, os pais ou respons�veis devem apresentar, no momento do check-in, documenta��o da crian�a (certid�o de nascimento, registro geral ou autoriza��o do juizado para viagem).

Esta mensagem pode conter links para websites que armazenam informa��es pessoais que possam lhe identificar. Starwood Hotels & Resorts Worldwide, Inc. n�o � legalmente respons�vel por a��es de websites independentes. Por favor verifique as pol�ticas de privacidade destes websites para entender como s�o coletadas, utilizadas e armazenadas tais informa��es.

Clique aqui<http://www.starwoodhotels.com/language.do?localeCode=en-US&returnURL=http://www.starwood.com/sheraton/privacy_policy.html> Para a Pol�tica de Privacidade da Starwood Hotels & Resorts Worldwide, Inc.'s.


Divulga��o:

Informa��o de Cancelamento
Para cancelar ou alterar uma reserva, favor entrar em contato com nosso departamento de reservas no telefone +55 (21) 3139 8000 ou ainda atrav�s de e-mail para reservas.barra@sheraton.com<http://www.starwoodhotels.com/sheraton/support/index.html>.

Tarifa / Validade da Reserva
Favor notar que a confirma��o eletr�nica � fornecida somente para voc� e para sua conveni�ncia. Mantemos os dados oficiais de sua reserva em nossos registros, incluindo detalhes das datas de sua estada e sua tarifa. Em caso de discrep�ncia, altera��es, modifica��es ou varia��es entre esta confirma��o e nossos registros, valer� o que consta em nossos registros. � estritamente proibido alterar as informa��es desta confirma��o, esta a��o estar� sujeita a implica��es legais.

Para os efeitos do artigo 5.II da Constitui��o Federal e do art. 6.III do C�digo de Defesa do Consumidor, a empresa declara que se reserva a faculdade de, a seu exclusivo crit�rio, recusar a aceita��o de pagamentos em cheques.
[http://www.starwoodhoteis.net/sistema_confirmacao/global/logos_rodape/brand_bar.gif]

